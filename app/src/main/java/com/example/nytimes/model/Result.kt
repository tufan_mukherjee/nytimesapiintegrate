package com.example.nytimes.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by tufan on 07,September,2019
 */

class Results {

  @SerializedName("bestsellers_date")
  @Expose
  var bestsellersDate: String? = null
  @SerializedName("lists")
  @Expose
  var lists: List<ListData>? = null
  @SerializedName("published_date")
  @Expose
  var publishedDate: String? = null

}
