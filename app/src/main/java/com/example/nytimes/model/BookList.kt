package com.example.nytimes.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by tufan on 07,September,2019
 */

class BookList {

  @SerializedName("copyright")
  @Expose
  var copyright: String? = null
  @SerializedName("num_results")
  @Expose
  var numResults: Int? = null
  @SerializedName("results")
  @Expose
  var results: Results? = null
  @SerializedName("status")
  @Expose
  var status: String? = null

}
