package com.example.nytimes.model

/**
 * Created by tufan on 07,September,2019
 */


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ListData {

  @SerializedName("books")
  @Expose
  var books: List<Book>? = null
  @SerializedName("display_name")
  @Expose
  var displayName: String? = null
  @SerializedName("list_id")
  @Expose
  var listId: Int? = null
  @SerializedName("list_image")
  @Expose
  var listImage: String? = null
  @SerializedName("list_name")
  @Expose
  var listName: String? = null
  @SerializedName("updated")
  @Expose
  var updated: String? = null

}
