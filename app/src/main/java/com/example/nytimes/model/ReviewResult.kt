package com.example.nytimes.model

/**
 * Created by tufan on 07,September,2019
 */


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReviewResult {

  @SerializedName("book_author")
  @Expose
  var bookAuthor: String? = null
  @SerializedName("book_title")
  @Expose
  var bookTitle: String? = null
  @SerializedName("byline")
  @Expose
  var byline: String? = null
  @SerializedName("isbn13")
  @Expose
  var isbn13: List<String>? = null
  @SerializedName("publication_dt")
  @Expose
  var publicationDt: String? = null
  @SerializedName("summary")
  @Expose
  var summary: String? = null
  @SerializedName("url")
  @Expose
  var url: String? = null

}
