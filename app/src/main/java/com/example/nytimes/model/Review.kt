package com.example.nytimes.model

/**
 * Created by tufan on 07,September,2019
 */


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Review {

  @SerializedName("copyright")
  @Expose
  var copyright: String? = null
  @SerializedName("num_results")
  @Expose
  var numResults: Int? = null
  @SerializedName("results")
  @Expose
  var results: List<ReviewResult>? = null
  @SerializedName("status")
  @Expose
  var status: String? = null

}
