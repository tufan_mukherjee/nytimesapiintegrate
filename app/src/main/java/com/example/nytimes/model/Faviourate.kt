package com.example.nytimes.model

/**
 * Created by tufan on 07,September,2019
 */


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.venturit.avidhrtconsumerapp.magic.TABLE_NAME_FAVIOURATE

@Entity(tableName = TABLE_NAME_FAVIOURATE)
data class Faviourate(
  @PrimaryKey(autoGenerate = true)
  var id: Int,

  @ColumnInfo(name = "isChecked") var isChecked: Boolean,
  @ColumnInfo(name = "publisher") var publisher: String,
  @ColumnInfo(name = "title") var title: String,
  @ColumnInfo(name = "description") var description: String,
  @ColumnInfo(name = "primary_isbn10") var primary_isbn10: String,
  @ColumnInfo(name = "rank") var rank: String,
  @ColumnInfo(name = "author") var author: String,
  @ColumnInfo(name = "contributor") var contributor: String
)
