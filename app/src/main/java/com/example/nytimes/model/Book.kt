package com.example.nytimes.model

/**
 * Created by tufan on 07,September,2019
 */


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Book {

  @SerializedName("age_group")
  @Expose
  var ageGroup: String? = null
  @SerializedName("author")
  @Expose
  var author: String? = null
  @SerializedName("contributor")
  @Expose
  var contributor: String? = null
  @SerializedName("contributor_note")
  @Expose
  var contributorNote: String? = null
  @SerializedName("created_date")
  @Expose
  var createdDate: String? = null
  @SerializedName("description")
  @Expose
  var description: String? = null
  @SerializedName("price")
  @Expose
  var price: Int? = null
  @SerializedName("primary_isbn10")
  @Expose
  var primaryIsbn10: String? = null
  @SerializedName("primary_isbn13")
  @Expose
  var primaryIsbn13: String? = null
  @SerializedName("publisher")
  @Expose
  var publisher: String? = null
  @SerializedName("rank")
  @Expose
  var rank: Int? = null
  @SerializedName("title")
  @Expose
  var title: String? = null
  @SerializedName("updated_date")
  @Expose
  var updatedDate: String? = null

}
