package com.example.nytimes

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nytimes.adapter.ParentAdapter
import com.example.nytimes.model.BookList
import com.example.nytimes.network.ApiServiceGenerator
import com.example.nytimes.network.ServiceCallApi
import com.venturit.avidhrtconsumerapp.magic.APIKEY
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import android.widget.EditText
import java.text.SimpleDateFormat
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {

  var myCalendar = Calendar.getInstance()
  var myFromCalender = Calendar.getInstance()

    @SuppressLint("ObsoleteSdkInt")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

      mRecyclerViewNYData.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL ,false)


      datePickerEdittext.setOnClickListener {

        val dpd = DatePickerDialog.newInstance(myDatePickerDialogStartDate, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)
        )
        dpd.show(supportFragmentManager, "Datepickerdialog");
        dpd.setAccentColor(ContextCompat.getColor(this, R.color.colorPrimary))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
          dpd.setMaxDate(myCalendar)
        }
      }
      updateLabel(datePickerEdittext, myCalendar)
    }
  var myDatePickerDialogStartDate:DatePickerDialog.OnDateSetListener = object:DatePickerDialog.OnDateSetListener {
    override fun onDateSet(view:DatePickerDialog, year:Int, monthOfYear:Int,
                           dayOfMonth:Int) {
      // TODO Auto-generated method stub
      myFromCalender.set(Calendar.YEAR, year)
      myFromCalender.set(Calendar.MONTH, monthOfYear)
      myFromCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth)
      mRecyclerViewNYData.setVisibility(View.GONE)
      updateLabel(datePickerEdittext, myFromCalender)
    }
  }
  private fun updateLabel(ed_date: EditText, myNewCalendar: Calendar) {
    val myFormat = "YYYY-MM-dd" //In which you need put here
    val sdf = SimpleDateFormat(myFormat, Locale.US)
    ed_date.setText(sdf.format(myNewCalendar.time))
    apiCall(ed_date.getText().toString().trim())
  }
  private fun apiCall(date: String) {

    val service = ApiServiceGenerator.createService(ServiceCallApi::class.java)
    val result = service.getListOfBook(APIKEY, date)
    result.enqueue(object : Callback<BookList> {
      override fun onResponse(call: Call<BookList>, response: Response<BookList>) {
        if (response != null) {
          if (response.body() != null) {
            try {

              parseJson(response.body())

            } catch (e: IOException) {
              e.printStackTrace()
            }

          } else {
            Log.i("onEmptyResponse", "Returned empty response")//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
          }
        }
      }

      override fun onFailure(call: Call<BookList>, t: Throwable) {

        // showHideRecordButton()
        centerLayout.visibility = View.VISIBLE

      }

    })
  }

  @SuppressLint("WrongConstant")
  private fun parseJson(bookList: BookList?) {
    mRecyclerViewNYData.visibility=View.VISIBLE


     var adapter = bookList?.results?.lists?.let { ParentAdapter(it, this) }
    mRecyclerViewNYData.adapter=adapter

  }
}
