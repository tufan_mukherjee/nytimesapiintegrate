package com.example.nytimes.network

import com.example.nytimes.model.BookList
import com.example.nytimes.model.Review
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceCallApi {
  @GET("svc/books/v3/lists/overview.json")
  fun getListOfBook(@Query("api-key") apiKey: String,@Query("published_date") published_date: String): Call<BookList>

  @GET("svc/books/v3/reviews.json")
   fun getReview(@Query("api-key") apikey: String,@Query("isbn") primaryISBN: String,@Query("title") title: String,@Query("author") author: String): Call<Review>


}
