package com.example.nytimes.network
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import com.google.gson.GsonBuilder




/**
 * Created by TUFAN on 07/09/2019.
 */

object ApiServiceGenerator {
  var APITIMEOUT: Long = 300
  val API_BASE_URL = "https://api.nytimes.com/"
  private val httpClient = OkHttpClient.Builder()
    .addInterceptor { chain ->
      val request = chain.request().newBuilder().build()
      chain.proceed(request)
    }
    //here we adding Interceptor for full level logging
    .addNetworkInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
    .readTimeout(APITIMEOUT, TimeUnit.SECONDS)
    .connectTimeout(APITIMEOUT, TimeUnit.SECONDS)
    .build()

  private val builder = Retrofit.Builder().baseUrl(API_BASE_URL)
          .addConverterFactory(ScalarsConverterFactory.create())
          .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))


  fun <S> createService(serviceClass: Class<S>): S {
    val retrofit = builder.client(httpClient).build()
    return retrofit.create(serviceClass)
  }

  fun retrofit(): Retrofit { // For Error Handing when non-OK response is received from Server
    val httpClient = OkHttpClient.Builder().build()
    val client = httpClient
    return builder.client(client).build()
  }



}