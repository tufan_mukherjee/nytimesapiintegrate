package com.example.nytimes.db.dao

/**
 * Created by tufan on 07,September,2019
 */
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

object DataRoomConverter {
  @TypeConverter
  fun toDate(value:Long):Date {
    return (if (value == null) null else Date(value))!!
  }
  @TypeConverter
  fun toLong(value:Date):Long {
    return (if (value == null) null else value.getTime())!!
  }
  @TypeConverter
  fun fromString(value:String):List<String> {
    val listType = object:TypeToken<List<String>>() {
    }.getType()
    return Gson().fromJson(value, listType)
  }
  @TypeConverter
  fun fromArrayList(list:List<String>):String {
    val gson = Gson()
    val json = gson.toJson(list)
    return json
  }
}