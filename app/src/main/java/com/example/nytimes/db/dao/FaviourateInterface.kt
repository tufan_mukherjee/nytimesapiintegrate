package com.example.nytimes.db.dao

import androidx.room.*
import com.example.nytimes.model.Faviourate
import com.venturit.avidhrtconsumerapp.magic.TABLE_NAME_FAVIOURATE

/**
 * Created by tufan on 07,September,2019
 */
@Dao
interface FaviourateInterface {
  @get:Query("SELECT * FROM $TABLE_NAME_FAVIOURATE WHERE isChecked=1 ORDER BY id DESC")
  val all: List<Faviourate>

  // @Query("SELECT * FROM "+ Constant.TABLE_NAME_TOURS +" WHERE tourID :tourID")
//  @Query("SELECT * FROM $TABLE_NAME_FAVIOURATE WHERE tourID = :arg0")
//  fun getFaviourateByTourID(arg0: Int): Faviourate


  /*
   * Insert the object in database
   * @param note, object to be inserted
   */

  @Insert
  fun insert(note: Faviourate)

  /*
   * update the object in database
   * @param note, object to be updated
   */
  @Update
  fun update(repos: Faviourate)

  /*
   * delete the object from database
   * @param note, object to be deleted
   */
  @Delete
  fun delete(faviourate: Faviourate)

  /*
   * delete list of objects from database
   * @param note, array of objects to be deleted
   */
  @Delete
  fun delete(vararg artworkData: Faviourate)
}
