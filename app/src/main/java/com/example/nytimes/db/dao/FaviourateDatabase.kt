package com.example.nytimes.db.dao
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.nytimes.model.Faviourate
import com.venturit.avidhrtconsumerapp.magic.DB_NAME

/**
 * Created by tufan on 07,September,2019
 */

@Database(
  entities = [Faviourate::class],
  version = 1
)
@TypeConverters(DataRoomConverter::class)
abstract class FaviourateDatabase:RoomDatabase() {
  abstract val faviourate:FaviourateInterface
//  fun cleanUp() {
//    faviourateDatabase = null
//  }
  companion object {
    private lateinit var faviourateDatabase:FaviourateDatabase
    // synchronized is use to avoid concurrent access in multithred environment
    /*synchronized*/ fun getInstance(context:Context):FaviourateDatabase {
      if (null == faviourateDatabase)
      {
        faviourateDatabase = buildDatabaseInstance(context)
      }
      return faviourateDatabase
    }
    private fun buildDatabaseInstance(context:Context):FaviourateDatabase {
      return Room.databaseBuilder(context,
        FaviourateDatabase::class.java, DB_NAME).allowMainThreadQueries().build()
    }
  }
}