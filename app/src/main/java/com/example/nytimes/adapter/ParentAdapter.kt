package com.example.nytimes.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nytimes.MainActivity
import com.example.nytimes.R
import com.example.nytimes.model.ListData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.parent_recycler.view.*

/**
 * Created by tufan on 07,September,2019
 */


class ParentAdapter(private val parents: List<ListData>, private val mContext: Context) :    RecyclerView.Adapter<ParentAdapter.ViewHolder>(){
  private val viewPool = RecyclerView.RecycledViewPool()
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val v = LayoutInflater.from(parent.context).inflate(R.layout.parent_recycler,parent,false)
    return ViewHolder(v)
  }
  override fun getItemCount(): Int {
    return parents.size
  }

  override fun onBindViewHolder(holder: ViewHolder,position: Int) {
    val parent = parents[position]
    holder.textView.text = parent.listName
    Picasso.get().load(parent.listImage).into(holder.imageBanner);
    holder.imageBanner.visibility=View.GONE
    val childLayoutManager = LinearLayoutManager(holder.recyclerView.context, LinearLayout.HORIZONTAL, false)
    childLayoutManager.initialPrefetchItemCount = 4
    holder.recyclerView.apply {
      layoutManager = childLayoutManager
      adapter = parent.books?.let { ChildAdapter(it,mContext) }
      setRecycledViewPool(viewPool)
    }

  }


  inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
    val recyclerView : RecyclerView = itemView.rv_child
    val textView: TextView = itemView.textViewDisplay_name
    val imageBanner: ImageView = itemView.imageBanner
  }
}