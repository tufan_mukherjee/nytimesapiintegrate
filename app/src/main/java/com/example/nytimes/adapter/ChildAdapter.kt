package com.example.nytimes.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.nytimes.BookDetailsActivity
import com.example.nytimes.R
import com.example.nytimes.model.Book
import com.venturit.avidhrtconsumerapp.magic.*
import kotlinx.android.synthetic.main.child_recycler.view.*
import kotlinx.android.synthetic.main.child_recycler.view.bookTitle

/**
 * Created by tufan on 07,September,2019
 */


class ChildAdapter(private val children: List<Book>,private val mContext: Context)
  : RecyclerView.Adapter<ChildAdapter.ViewHolder>(){

  override fun onCreateViewHolder(parent: ViewGroup,
                                  viewType: Int): ViewHolder {

    val v =  LayoutInflater.from(parent.context)
      .inflate(R.layout.child_recycler,parent,false)
    return ViewHolder(v)
  }

  override fun getItemCount(): Int {
    return children.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val child = children[position]
    holder.bookTitle.text = child.title
    holder.bookAuthor.text = "By "+child.title

    holder.itemView.setOnClickListener {
      val intent = Intent(mContext, BookDetailsActivity::class.java)
// To pass any data to next activity
      intent.putExtra(primary_isbn10, child.primaryIsbn10)
      intent.putExtra(primary_isbn13, child.primaryIsbn13)
      intent.putExtra(bookTitleIntent, child.title)
      intent.putExtra(auhtor, child.author)
// start your next activity
      mContext.startActivity(intent)
    }
  }


  inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

    val bookTitle : TextView = itemView.bookTitle
    val bookAuthor: TextView = itemView.bookAuthor



  }
}