package com.example.nytimes

import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.example.nytimes.model.BookList
import com.example.nytimes.model.Review
import com.example.nytimes.network.ApiServiceGenerator
import com.example.nytimes.network.ServiceCallApi
import com.venturit.avidhrtconsumerapp.magic.*

import kotlinx.android.synthetic.main.activity_book_details.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_book_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class BookDetailsActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_book_details)
    setSupportActionBar(toolbar)


    apiCall(intent.getStringExtra(primary_isbn13),intent.getStringExtra(bookTitleIntent),intent.getStringExtra(auhtor))

    toolbar.setTitle(intent.getStringExtra(bookTitleIntent))
  }
  private fun apiCall(primaryISBN: String,title: String,author: String) {

    val service = ApiServiceGenerator.createService(ServiceCallApi::class.java)
    val result = service.getReview(APIKEY, primaryISBN,title,author)
    result.enqueue(object : Callback<Review> {
      override fun onResponse(call: Call<Review>, response: Response<Review>) {
        if (response != null) {
          if (response.body() != null) {
            try {

              parseJson(response.body())

            } catch (e: IOException) {
              e.printStackTrace()
            }

          } else {
            Log.i("onEmptyResponse", "Returned empty response")//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
          }
        }
      }

      override fun onFailure(call: Call<Review>, t: Throwable) {

        Log.i("onEmptyResponse", t.message)

      }

    })
  }

  private fun parseJson(review: Review?) {

    if (review?.results!!.size>0){
      bookTitle.text= review.results?.get(0)?.bookTitle
    }

//    bookTitle.text= review?.results?.get(0)?.bookTitle ?:
//    bookTitle.text= review?.results?.get(0).bookTitle
  }
}
